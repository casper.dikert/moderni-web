<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title></title>
  </head>
  <body>
    <div class="wrapper">
      <div class="top-nav">
        <ul>
          <?php
          require_once 'connect.php';

          session_start();

          if (isset($_SESSION['s_id'])) {
            echo '<li><a href="logout.php">Kirjaudu ulos</a></li>';
          } else {
            echo '<li><a href="register.php">Rekisteröidy</a></li>';
            echo '<li><a href="login.php">Kirjaudu</a></li>';
          }?>
          <li><a href="index.php">Etusivu</a></li>
        </ul>
      </div>

      <header>
        <h1>
          <?php
           $cat =  mysqli_real_escape_string($yhteys, strip_tags($_GET['cat']));
           echo $cat;
          ?>
        </h1>

      </header>

      <div class="questions">

       <?php
        $sql = 'SELECT * FROM kysymys';


        if (isset($_GET['cat'])) {
          $cat =  mysqli_real_escape_string($yhteys, strip_tags($_GET['cat']));
          $sql .= " WHERE kategoria = '$cat'";
        }
        $tulokset = $yhteys->query($sql);
        if ($tulokset->num_rows > 0) {

          while($rivi = $tulokset->fetch_assoc()) {
            $sisalto = $rivi['sisalto'];
            if (strlen($sisalto) > 100) {
              $sisalto = substr($sisalto, 0 , 100). "...";
            }
            echo "<a href = 'view_question.php?id=" . $rivi['kysymysID'] . "'>";
            echo "<div class ='one'>";
            echo "<p class = 'small'>";
            echo $rivi["nimimerkki"]. " ";
            $date=date_create($rivi['paivamaara']);
            echo date_format($date,"d.m.Y ");
            echo "</p>";
            echo "<p class = 'headline'>";
            echo "<b>";
            echo $rivi["otsikko"] . '<br>';
            echo "</b>";
            echo "</p>";
            echo "<p class = 'content'>";
            echo $sisalto . '<br>';
            echo "</p>";
            echo "</a>";
            echo "</div>";
            }

        } else {
          echo "Ei tuloksia";
        }
       ?>
     </div>
  </body>
</html>
