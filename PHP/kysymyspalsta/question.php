<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <title></title>
  </head>
  <body>
    <div class="wrapper">
      <div class="top-nav">
        <ul>
          <?php
          require_once 'connect.php';

          session_start();

          if (isset($_SESSION['s_id'])) {
            echo '<li><a href="logout.php">Kirjaudu ulos</a></li>';
          } else {
            echo '<li><a href="register.php">Rekisteröidy</a></li>';
            echo '<li><a href="login.php">Kirjaudu</a></li>';
          }?>
          <li><a href="index.php">Etusivu</a></li>
        </ul>
      </div>

      <header>
        <h1>Postaus</h1>

      </header>
      <div class="login">
        <form class="" action="question.php" method="post">

          <label for="otsikko">Otsikko:</label> <br>
          <input type="text" <?php if (isset($_GET['otsikko'])) {
            echo "style = 'border:  1px solid #ff3f3f;'";
          } ?> name="otsikko" value=""> <br>

          <label for="sisalto">Sisältö:</label> <br>
          <textarea name="sisalto" rows="8" cols="50" <?php if (isset($_GET['sisalto'])) {
            echo "style = 'border:  1px solid #ff3f3f;'";} ?>></textarea>
             <br>

          <label for="kategoria">Kategoria:</label> <br>
          <select id="category" name="kategoria">
             <option value="valitse" disabled selected>Valitse kategoria</option>
             <option value="autot">Autot</option>
             <option value="harrastukset">Harrastukset</option>
             <option value="matkailu">Matkailu</option>
             <option value="ruokajajuoma">Ruoka ja juoma</option>
             <option value="terveys">Terveys</option>
             <option value="urheilujakuntoilu">Urheilu ja kuntoilu</option>
             <option value="viihdejakulttuuri">Viihde ja kulttuuri</option>
             <option value="jokinmuu">Jokin muu</option>

          </select>

            <input type="submit" name="submit" value="Lähetä">
          </form>
        </div>
            <?php

            if(isset($_GET['kategoria']) && $_GET['kategoria'] ==  'valitse' ) {
              echo "<p class = 'notification3'>Valitse kategoria!</p>";
            }
            if(isset($_GET['lahetys']) && $_GET['lahetys'] ==  'onnistui' ) {
              echo "<p class = 'notification3'>Lähetys onnistui!</p>";
            }

                if (isset($_POST['submit'])) {

                  $otsikko =  mysqli_real_escape_string($yhteys, strip_tags($_POST['otsikko']));
                  $sisalto =  mysqli_real_escape_string($yhteys, strip_tags($_POST['sisalto']));
                  $kategoria = mysqli_real_escape_string($yhteys, strip_tags($_POST['kategoria']));
                  $nimi = $_SESSION['s_nimi'];
                  $pvamra = date("Y-m-d");
                  if (empty($otsikko)) {
                    $virheilmoitus = "otsikko";

                    header('Location: question.php?' . $virheilmoitus);
                    exit();
                  }

                  if ($kategoria == '') {
                    header('Location: question.php?kategoria=valitse');
                    exit();
                  }

                  $sql2 = "INSERT INTO kysymys (nimimerkki, otsikko, sisalto, paivamaara, kategoria) VALUES('$nimi', '$otsikko', '$sisalto', '$pvamra', '$kategoria' );";
                  mysqli_query($yhteys, $sql2);
                  header('Location: question.php?lahetys=onnistui');

                }
            ?>
    </div>
  </body>
</html>
