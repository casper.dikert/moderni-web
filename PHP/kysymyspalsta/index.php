<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">

    <title></title>
  </head>
  <body>

    <div class="wrapper">
      <div class="top-nav">
        <ul>
          <?php
          require_once 'connect.php';

          session_start();

          if (isset($_SESSION['s_id'])) {
            echo '<li><a href="logout.php">Kirjaudu ulos</a></li>';
          } else {
            echo '<li><a href="register.php">Rekisteröidy</a></li>';
            echo '<li><a href="login.php">Kirjaudu</a></li>';
          }?>
          <li><a href="index.php">Etusivu</a></li>
        </ul>
      </div>

      <header>
        <h1>Joku's palsta</h1>
        <p>Kysy ja joku vastaa! <br>
        <a href=""><img  src="shopping.png" width="20px" alt=""></a>
        </p>
      </header>
      <?php
      if (isset($_SESSION['s_id'])) {
        echo '<p class="welcome">Tervetuloa:  '. $_SESSION["s_nimi"];
      }
      ?>
    </p>

      <div class="categories">
        <ul>
          <li><a href="category.php?cat=autot">Autot</a></li>
          <li><a href="category.php?cat=harrastukset">Harrastukset</a></li>
          <li><a href="category.php?cat=matkailu">Matkailu</a></li>
          <li><a href="category.php?cat=ruokajajuoma">Ruoka ja juoma</a></li>
          <li><a href="category.php?cat=terveys">Terveys</a></li>
          <li><a href="category.php?cat=urheilujakuntoilu">Urheilu ja kuntoilu</a></li>
          <li><a href="category.php?cat=viihdejakulttuuri">Viihde ja kulttuuri</a></li>
          <li><a href="category.php?cat=jokinmuu">Jokin muu</a></li>
        </ul>
      </div>
      <?php
        if (!isset($_SESSION['s_id'])) {
          echo "<div class='question'>

          <a href='login.php'>Luo postaus</a>
          </div>";

      } else {
        echo "<div class='question'>
        <a href='question.php'>Luo postaus</a>
        </div>";

      }
        ?>
    </div>
  </body>
</html>
