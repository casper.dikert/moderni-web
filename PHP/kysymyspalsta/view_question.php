<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <title></title>
  </head>
  <body>
    <div class="wrapper">
      <div class="top-nav">
        <ul>
          <?php
          require_once 'connect.php';

          session_start();

          if (isset($_SESSION['s_id'])) {
            echo '<li><a href="logout.php">Kirjaudu ulos</a></li>';
          } else {
            echo '<li><a href="register.php">Rekisteröidy</a></li>';
            echo '<li><a href="login.php">Kirjaudu</a></li>';
          }?>
          <li><a href="index.php">Etusivu</a></li>
        </ul>
      </div>
      <div class="view_question">
        <header>
          <h1>
            <?php
            $sql = 'SELECT * FROM kysymys';

            if (isset($_GET['id'])) {
              $id =  mysqli_real_escape_string($yhteys, strip_tags($_GET['id']));
              $sql .= " WHERE kysymysID = '$id'";
            }

            $tulokset = $yhteys->query($sql);
            if ($tulokset->num_rows > 0) {
              $rivi = $tulokset->fetch_assoc();
                echo $rivi['otsikko'];

          echo "</h1>";
        echo "</header>";

          echo "<p class = 'namedate'>";
          echo $rivi["nimimerkki"]. " ";
          $date=date_create($rivi['paivamaara']);
          echo date_format($date,"d.m.Y ");
          echo "</p>";
          echo "<p class = 'view_question_content'>";
          echo $rivi['sisalto'];
        }
           ?>
         </p>

      </div>

      <?php

       $sql2 = 'SELECT * FROM vastaus';
       if (isset($_GET['id'])) {
         $id2 =  mysqli_real_escape_string($yhteys, strip_tags($_GET['id']));
         $sql2 .= " WHERE kysymysID = '$id2'";
       }
       $tulokset = $yhteys->query($sql2);
       if ($tulokset->num_rows > 0) {
         while($rivi2 = $tulokset->fetch_assoc()) {
           echo "<div class = 'user_answer'>";
           echo "<p class = 'namedate2'>";
           echo $rivi2["nimimerkki"]. " ";
           $date=date_create($rivi2['paivamaara']);
           echo date_format($date,"d.m.Y ");
           echo "</p>";
           echo $rivi2['sisalto'] ;
           echo "</div>";
         }
       }
       ?>

      <div class="answer">
        <p>Vastaa alkuperäiseen viestiin</p>
        <form class=""<?php echo "action='view_question.php?id=" . $rivi['kysymysID'] . "'"; ?> method="post">

          <label for="sisalto">Viesti: </label> <br>
          <textarea name="sisalto" rows="8" cols="50" <?php if (isset($_GET['sisalto'])) {
            echo "style = 'border:  1px solid #ff3f3f;'";} ?>></textarea>

            <?php

            if (isset($_SESSION['s_id'])) {
              echo '<input type="submit" name="submit" value="Lähetä">';
            } else {
              echo "<p class = 'notification4'>Kirjaudu sisään lähettääksesi vastauksen </p>";
            }

            ?>
          </form>
        </div>
          <?php

          if(isset($_GET['lahetys']) && $_GET['lahetys'] ==  'onnistui' ) {
            echo "<p class = 'notification3'>Lähetys onnistui!</p>";
          }
            if (isset($_POST['submit'])) {

              $sisalto =  mysqli_real_escape_string($yhteys, strip_tags($_POST['sisalto']));
              $nimi = $_SESSION['s_nimi'];
              $pvamra = date("Y-m-d");
              if (isset($_GET['id'])) {
                $id =  mysqli_real_escape_string($yhteys, strip_tags($_GET['id']));

              }

              if (empty($sisalto)) {
                echo "<p class = 'notification2'>Kirjoita tekstikenttään vastauksesi</p>";
                exit();
              }

              $sql = "INSERT INTO vastaus ( kysymysID, nimimerkki, sisalto, paivamaara) VALUES('$id', '$nimi', '$sisalto', '$pvamra');";
              mysqli_query($yhteys, $sql);
              header('Location: view_question.php?id=' . $rivi["kysymysID"] . "&lahetys=onnistui");
            }
            ?>
    </div>
  </body>
</html>
