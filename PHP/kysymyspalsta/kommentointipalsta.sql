-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 06.02.2019 klo 08:49
-- Palvelimen versio: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kommentointipalsta`
--

-- --------------------------------------------------------

--
-- Rakenne taululle `kayttaja`
--

CREATE TABLE `kayttaja` (
  `kayttajaID` int(11) NOT NULL,
  `sahkoposti` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `nimimerkki` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `Salasana` varchar(250) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Vedos taulusta `kayttaja`
--

INSERT INTO `kayttaja` (`kayttajaID`, `sahkoposti`, `nimimerkki`, `Salasana`) VALUES
(11, 'admin@gmail.com', 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
(14, 'apua@hotmail.com', 'apua', '7d2a56f162bb4fea170268697de607618abf9d16468f6ded345c2147c7bcf01a'),
(15, 'admin2@gmail.com', 'admin2', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
(16, 'jammu@gmail.com', 'Jammunen', '577c568654b3877f7cae6b0f74a71b8f5cc09344595a88f4ca4183e73e469bba');

-- --------------------------------------------------------

--
-- Rakenne taululle `kysymys`
--

CREATE TABLE `kysymys` (
  `kysymysID` int(11) NOT NULL,
  `nimimerkki` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `otsikko` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `sisalto` text COLLATE utf8_swedish_ci NOT NULL,
  `paivamaara` date NOT NULL,
  `kategoria` varchar(50) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Vedos taulusta `kysymys`
--

INSERT INTO `kysymys` (`kysymysID`, `nimimerkki`, `otsikko`, `sisalto`, `paivamaara`, `kategoria`) VALUES
(1, 'admin2', 'Auton renkaan vaihto?', 'apua3', '2019-01-30', 'autot'),
(2, 'admin2', 'Hyvä kanalaatikko?', 'Etsin hyää reseptiä kanalaatikolle?', '2019-01-30', 'ruokajajuoma'),
(7, 'admin2', 'Matkakohteita', 'Matkailun kielteisiä seurauksia ovat etenkin kehittyvissä maissa talouden rakenteiden vääristyminen, ympäristöhaitat ja nähtävyyksien rapistuminen ja kielteiset kulttuurisiirtymät. Matkailu on lisääntynyt voimakkaasti viime vuosikymmenenä, mikä on nopeuttanut alkuperäiskulttuurien tuhoutumista ja luonnon saastumista lisääntyneen liikenteen takia.\r\n\r\nJoissain Etelä-Euroopan kaupungeissa kuten Venetsiassa, Barcelonassa ja Lissabonissa on 2000-luvulla voimakkaasti lisääntynyt matkailu aiheuttanut muun muassa vuokrien nousua ja häiriöitä, mikä on johtanut protesteihin ja alkuperäisten asukkaiden poismuuttoon.', '2019-01-31', 'matkailu'),
(8, 'admin2', 'Foodora vs Wolt', 'Olemme Woltin suurkuluttajia. Tänään Turussa lankesin Foodoraan, koska kuljettivat ruuat ilman maksua. Ei olisi (taaskaan) pitänyt!\r\n\r\nEn saa linkitettyä keskustelua ASPAn kanssa tähän; siitähän se palvelun pöyristyttävyys olisi näkynyt suoraan.\r\n\r\nPuolet safkoista puuttui. Loppuja ei toimitettu, eikä asiaa mitenkään hyvitetty. \r\n\r\nAika kylmää toimintaa. Odotettiin yli tunti, ja puolet jengistä jäi ruuatta. \r\n\r\nAiemminkin ovat mokanneet eivätkä ole mokaansa korjanneet tai hyvittäneet.\r\n\r\nWoltista mulla on pelkästään hyviä kokemuksia, virheisiin suhtaudutaan asianmukaisesti.', '2019-02-05', 'ruokajajuoma'),
(9, 'Jammunen', 'Norwegian lopettaa reittejä', 'Norwegian hakee suuria kulusäästöjä ja aikoo sulkea toimipisteitä ja lopettaa kannattamattomia reittejä ja siirtää koneita ja miehistöjä paremmin kannattaville. Lentoyhtiö kertoi jo lokakuussa hakevansa yli kahdensadan miljoonan euron säästöjä.\r\n\r\nEspanjassa suljetaan vuoden loppuun mennessä Palma de Mallorcan, Gran Canarian ja Teneriffan toimipisteet, Italiassa Rooma ja Yhdysvalloissa Stewart ja Providence. \r\n\r\nhttps://www.savonsanomat.fi/talous/Norjalaislehti-Säästöjä-metsästävä-Norwegian-lopettaa-reittejä-ja-sulkee-toimipisteitä-etelän-lomakohteissa/1311932\r\n', '2019-02-05', 'matkailu'),
(10, 'Jammunen', 'Kalkaros ei ole paha', 'Nyt taas tuli ajateltua Harry Potteria kun luin sitä suomeksi. Tulin siihen tulokseen, että Kalkaros ei ole paha. Ja persustelut tulevat tässä: Kalkaroksen oli pakko tappaa Dumbledore tilanteessa, jossa aikaisemmin vannottu rikkumaton vala(Unbreakable Vow) velvoitti Kalkarosta. Ja Dumbledore olisi varmasti ennaltakäsin suostunut kuolemaansa, jos olisi samalla pystynyt takaamaan \"hyviksille\" sen edun, että Kalkaros saa kuolonsyöjien ja ehkä Voldemortinkin täyden luottamuksen(samalla menetti kyllä täydellisesti hyviksien luottamuksen, mutta ehkä Kalkaros pystyy vielä selittämään Harrylle samalla lailla kuin Siriuskin aikoinaan). Sitten vielä tämä \"Severus,please\". Aika käsittämättömältä tuntuisi se, että Dumbledore anelisi armoa. Jokin muu siinä oli takana. Jokin aikaisemmin sovittu asia aivan selvästi.\r\nNäiden todisteiden valossa pyydän teiltä, että älkää langettako vielä syytöksiänne Kalkarosta kohtaan, ehkä kaikki selviääkin vasta 7. kirjassa.', '2019-02-05', 'viihdejakulttuuri');

-- --------------------------------------------------------

--
-- Rakenne taululle `vastaus`
--

CREATE TABLE `vastaus` (
  `vastausID` int(11) NOT NULL,
  `kysymysID` int(11) NOT NULL,
  `nimimerkki` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `sisalto` text COLLATE utf8_swedish_ci NOT NULL,
  `paivamaara` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Vedos taulusta `vastaus`
--

INSERT INTO `vastaus` (`vastausID`, `kysymysID`, `nimimerkki`, `sisalto`, `paivamaara`) VALUES
(1, 8, 'Jammunen', 'MENE TÖIHIN5\r\n', '2019-02-05'),
(3, 8, 'Jammunen', 'MEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE NYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYT TTTTTTTTTTTTTTTTTTÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖIIIIIIIIIIIIHHHHHIIIIN', '2019-02-05'),
(4, 8, 'Jammunen', 'Melkein kuin minun viestini. :)\r\n\r\nItkin myös kirjan lopussa. Surullinen ja järkyttävä. \r\n\r\nOlen kahden vaiheilla, että onko Kalkaros sit hyvä vaiko paha. Kalkaros on suosikkini, mutta nyt en osaa sanoa pidänkö hänestä. Jään odottelemaan sitä 7. kirjaa. Sisimmissäni toivon, että Kalkaros olisi edelleen hyvä.', '2019-02-06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kayttaja`
--
ALTER TABLE `kayttaja`
  ADD PRIMARY KEY (`kayttajaID`);

--
-- Indexes for table `kysymys`
--
ALTER TABLE `kysymys`
  ADD PRIMARY KEY (`kysymysID`);

--
-- Indexes for table `vastaus`
--
ALTER TABLE `vastaus`
  ADD PRIMARY KEY (`vastausID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kayttaja`
--
ALTER TABLE `kayttaja`
  MODIFY `kayttajaID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `kysymys`
--
ALTER TABLE `kysymys`
  MODIFY `kysymysID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `vastaus`
--
ALTER TABLE `vastaus`
  MODIFY `vastausID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
