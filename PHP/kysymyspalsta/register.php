<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">

    <title></title>
  </head>
  <body>

    <div class="wrapper">
      <div class="top-nav">
        <ul>
          <li><a href="register.php">Rekisteröidy</a></li>
          <li><a href="login.php">Kirjaudu</a></li>
          <li><a href="index.php">Etusivu</a></li>
        </ul>
      </div>

      <header>
        <h1>Rekisteröityminen</h1>
      </header>
      <div class="container">

        <div class="text_register">
          <p>Sinun pitää rekisteöityä voidaksesi käytää nettisivun kaikkia ominaisuuksia kuten kysymysten kysyminen ja niihin vastaaminen.</p>
        </div>

        <div class="register">
          <form class="" action="register.php" method="post">

            <label for="nimimerkki">Nimimerkki:</label> <br>
            <input type="text" <?php if (isset($_GET['nimi'])) {
              echo "style = 'border:  1px solid #ff3f3f;'";
            } ?> name="nimimerkki" value=""> <br>

            <label for="salasana">Salasana:</label> <br>
            <input type="password" <?php if (isset($_GET['salasana'])) {
              echo "style = 'border:  1px solid #ff3f3f;'";
            } ?>  name="salasana" value=""> <br>

            <label for="sahkoposti">Sähköposti:</label> <br>
            <input type="email" <?php if (isset($_GET['sposti'])) {
              echo "style = 'border:  1px solid #ff3f3f;'";
            } ?> name="sahkoposti" value=""> <br>

            <br>
            <input type="submit" name="submit" value="Luo tunnus">

            <?php if(isset($_GET['rekisteroityminen']) && $_GET['rekisteroityminen'] ==  'onnistui' ) {
                    echo "<p class = 'notification'>Rekisteröityminen onnistui!</p>";
                  }

                  if(isset($_GET['kayttaja']) && $_GET['kayttaja'] ==  'varattu' ) {
                    echo "<p class = 'notification'>Nimimerkki on jo käytössä!</p>";
                  }

                  if(isset($_GET['sahkoposti']) && $_GET['sahkoposti'] ==  'kaytossa' ) {
                    echo "<p class = 'notification'>Sähköposti on jo käytössä!</p>";
                  }

                  if(isset($_GET['salasana']) && $_GET['salasana'] ==  'lyhyt' ) {
                    echo "<p class = 'notification2'>Salasana liian lyhyt min. 5 merkkiä!</p>";
                  }
            ?>
          </form>
        </div>
        <?php

        require_once 'connect.php';

          if (isset($_POST['submit'])) {
            $nimimerkki =  mysqli_real_escape_string($yhteys, strip_tags($_POST['nimimerkki']));
            $sahkoposti =  mysqli_real_escape_string($yhteys, strip_tags($_POST['sahkoposti']));
            $salasana =  mysqli_real_escape_string($yhteys, strip_tags($_POST['salasana']));

            if (empty($nimimerkki) || empty($salasana) || empty($sahkoposti) || strlen($salasana) < 5) {
              $virheilmoitus = "";

              if (empty($nimimerkki)) {
                 $virheilmoitus.= "nimi";
              }
              if (empty($sahkoposti)) {
                if (!empty($virheilmoitus)) {
                  $virheilmoitus.="&";
                }
                $virheilmoitus.= "sposti";
              }
              if (empty($salasana)) {
                if (!empty($virheilmoitus)) {
                  $virheilmoitus.="&";
                }
                $virheilmoitus.= "salasana";
              }

              if (!empty($salasana) && strlen($salasana) < 5) {
                if (!empty($virheilmoitus)) {
                  $virheilmoitus.="&";
                }
                $virheilmoitus.= "salasana=lyhyt";
              }

              header('Location: register.php?' . $virheilmoitus);
              exit();
            }

            $sqlhaku = "SELECT * FROM kayttaja WHERE nimimerkki = '$nimimerkki'";
            $tulos = mysqli_query($yhteys, $sqlhaku);
            $tulostarkistus = mysqli_num_rows($tulos);

            $sqlhaku2 = "SELECT * FROM kayttaja WHERE sahkoposti = '$sahkoposti'";
            $tulos2 = mysqli_query($yhteys, $sqlhaku2);
            $tulostarkistus2 = mysqli_num_rows($tulos2);

            if ($tulostarkistus2 > 0) {
              header('Location: register.php?sahkoposti=kaytossa');
              exit();
            }
            if ($tulostarkistus > 0) {
              header('Location: register.php?kayttaja=varattu');
              exit();
            } else {
              echo "$salasana";
              $hashedsalasana = hash('sha256', $salasana);
              $sql = "INSERT INTO kayttaja (sahkoposti, nimimerkki, Salasana) VALUES('$sahkoposti', '$nimimerkki', '$hashedsalasana');";
              mysqli_query($yhteys, $sql);
              header('Location: register.php?rekisteroityminen=onnistui');
            }
          }
         ?>
      </div>
    </div>
  </body>
</html>
