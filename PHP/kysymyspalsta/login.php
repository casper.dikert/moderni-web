<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title></title>
  </head>
  <body>
    <div class="wrapper">
      <div class="top-nav">
        <ul>
          <?php
          require_once 'connect.php';

          session_start();
          
          if (isset($_SESSION['s_id'])) {
            echo '<form class="" action="logout.php" method="post">
            <li><input type="submit" name="submit" value="Kirjaudu ulos"></li>';
          } else {
            echo '<li><a href="register.php">Rekisteröidy</a></li>';
            echo '<li><a href="login.php">Kirjaudu</a></li>';
          }?>
          <li><a href="index.php">Etusivu</a></li>
        </ul>
      </div>

      <header>
        <h1>Kirjaudu sisään</h1>
      </header>

      <div class="login">
        <form class="" action="login.php" method="post">

          <label for="sahkoposti">Sähköposti:</label> <br>
          <input type="email" <?php if (isset($_GET['sposti'])) {
            echo "style = 'border:  1px solid #ff3f3f;'";
          } ?> name="sahkoposti" value=""> <br>

          <label for="salasana">Salasana:</label> <br>
          <input type="password" <?php if (isset($_GET['salasana'])) {
            echo "style = 'border:  1px solid #ff3f3f;'";
          } ?>  name="salasana" value=""> <br>

            <input type="submit" name="submit" value="Kirjaudu sisään">

            <?php if(isset($_GET['kirjautuminen']) && $_GET['kirjautuminen'] ==  'epaonnistui' ) {
                    echo "<p class = 'notification'>Kirjautuminen epäonnistui!</p>";
                  }
            ?>

            <?php

              if (isset($_POST['submit'])) {
                $sahkoposti = mysqli_real_escape_string($yhteys, $_POST['sahkoposti']);
                $salasana = mysqli_real_escape_string($yhteys, $_POST['salasana']);


                if (empty($sahkoposti) || empty($salasana)) {
                  $virheilmoitus = "";

                  if (empty($sahkoposti)) {
                     $virheilmoitus.= "sposti";
                  }
                  if (empty($salasana)) {
                    if (!empty($virheilmoitus)) {
                      $virheilmoitus.="&";
                    }
                    $virheilmoitus.= "salasana";
                  }

                  header('Location: login.php?' . $virheilmoitus);
                  exit();
                }

                  $hashedsalasana = hash ('sha256', $salasana);
                  $sqlhaku = "SELECT * FROM kayttaja WHERE sahkoposti = '$sahkoposti' and salasana = '$hashedsalasana'";
                  $tulos = mysqli_query($yhteys, $sqlhaku);
                  $tulostarkistus = mysqli_num_rows($tulos);

                  if ($tulostarkistus < 1) {
                    header ('Location: login.php?kirjautuminen=epaonnistui');
                    exit();
                  } else {
                    if ($rivi = mysqli_fetch_assoc($tulos)) {

                      $_SESSION['s_id'] = $rivi['kayttajaID'];
                      $_SESSION['s_sposti'] = $rivi['sahkoposti'];
                      $_SESSION['s_nimi'] = $rivi['nimimerkki'];

                      header('Location: index.php?kirjautuminen=onnistui');
                      exit();
                    }
                  }
                }

             ?>
        </form>
      </div>
    </div>
  </body>
</html>
